
import Link from "next/link"
import React from "react"
import ThemeSwitch from "./ThemeSwitch";
import fr_icon from "../public/img/fr_icon.png";
import en_icon from "../public/img/en_icon.png";
import Image from "next/image";
import { FaLinkedin } from "react-icons/fa";
import { RiGitlabFill } from "react-icons/ri";
import { BsTwitter } from "react-icons/bs";
import { useTranslation } from 'react-i18next';

const Navigation = () => {

    const { i18n } = useTranslation('home');
  
    return (
        <div className="sticky top-0 z-20 py-2 bg-white md:py-6 md:mb-6 dark:bg-black">
            <div className="container px-4 mx-auto lg:max-w-7xl flex items-center justify-between">
                <div className="flex flex-row w-30 justify-between">

                    <Link href="/">
                        <a target="_blank"
                            className={"font-medium tracking-wider transition-colors text-[#2F1C6A] hover:text-gray-900 uppercase dark:text-white mx-2"}
                        >
                            Petito Tapondjou
                        </a>
                    </Link>

                    <Link href="https://linkedin.com/in/petito-tapondjou-3803b915b">
                        <a target="_blank"
                            className={"font-medium tracking-wider transition-colors hover:text-[#2F1C6A] dark:text-white mx-2"}
                        >
                            <FaLinkedin size={25} />
                        </a>
                    </Link>

                    <Link href="https://github.com/lebresilien">
                        <a target="_blank"
                            className={"font-medium tracking-wider transition-colors hover:text-[#2F1C6A] dark:text-white mx-2"}
                        >
                            <RiGitlabFill size={25} />
                        </a>
                    </Link>

                    <Link href="https://twitter.com/lebresilien237">
                        <a
                            className={"font-medium tracking-wider transition-colors hover:text-[#2F1C6A] text-gray-900 dark:text-white"}
                        >
                            <BsTwitter size={25} />
                        </a>
                    </Link>
                </div>
               
                <div className="w-70 cursor-pointer">
                    
                    {
                        i18n.language == "fr" ?
                        <Link href={'/'} locale={'en'}>
                            <a>
                                <Image
                                        src={en_icon}
                                        alt="lang"
                                        priority={true}
                                        className="rounded-full"
                                        width={25}
                                        height={25}
                                />
                            </a>
                        </Link>:

                        <Link href={'/'} locale={'fr'}>
                            <a>
                                <Image
                                        src={fr_icon}
                                        alt="lang"
                                        priority={true}
                                        className="rounded-full"
                                        width={27}
                                        height={27}
                                />
                            </a>
                        </Link>

                    }
                  
                    <ThemeSwitch />
                </div>
            </div>
        </div>
    )
}

export default Navigation;