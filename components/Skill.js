import React from "react";
import { useTranslation } from "react-i18next";
import { DiAndroid, DiTerminal, DiApple } from "react-icons/di";
import { SiPhp, SiMysql, SiGit, SiTrello } from "react-icons/si";

function Skill() {

    const { t } = useTranslation('common');
  
    const skills =  [
            {
                slug: "android",
                Component: DiAndroid,
                title: "Android",
                Description: () => <> {t('portofolio.skill.desc_1')}</>,
              },
              {
                slug: "apple",
                Component: DiApple,
                title: "Apple",
                Description: () => <>{t('portofolio.skill.desc_2')} </>,
              },
              {
                slug: "terminal",
                Component: DiTerminal,
                title: "Web developpment",
                Description: () => <>{t('portofolio.skill.desc_3')}</>,
              },
              {
                slug: "Mysql",
                Component: SiMysql,
                title: "Mysql",
                Description: () => <>{t('portofolio.skill.desc_4')}</>,
              }, 
              {
                slug: "trello",
                Component: SiTrello,
                title: "Trello",
                Description: () => <>{t('portofolio.skill.desc_5')}</>,
              },
              {
                slug: "git",
                Component: SiGit,
                title: "Git",
                Description: () => <>{t('portofolio.skill.desc_6')}</>,
              },
    ]

    const SkillCard = ({title, Description, Component}) => (
        <div className="flex flex-col my-5 basis-1/3 pr-5">

            <div className="flex flex-row dark:bg-gray-900">
                <div className="basis-1/12 pt-2">
                    <Component size={25}/>
                </div>
                <div className="basis-11/12  dark:text-white md:text-2xl text-xl">
                    { title }
                </div>
            </div>

            <div className="flex flex-row">
                <div className="basis-1/12"></div>
                <div className="basis-11/12 dark:text-white md:text-2xl text-xl">
                    <Description />
                </div>
            </div>
        </div>
    );

    return (
        <div className="container px-4 mx-auto text-[#2F1C6A] dark:text-white">
            <div className="lg:space-x-5 item-center lg:-mx-4 flex flex-col">
                
                <div className="lg:px-4 lg:mt-12">
                    <h1 className="text-2xl font-bold lg:text-5xl dark:text-white">
                      {t('portofolio.skill.title')}
                    </h1>
                </div>

                <div className="mt-6 dark:text-white md:text-2xl text-xl">
                    <p className="mb-4">
                        {t('portofolio.skill.description')}
                    </p>
                </div>

                <div className="flex md:flex-row md:flex-wrap flex-col my-5">
                   { 
                      skills.map((skill, index) => (
                        <SkillCard 
                            title={skill.title}
                            Description={skill.Description}
                            Component={skill.Component}
                            key={index}
                        />
                      ))
                   }
                </div>
            </div>
        </div>
    )
}

export default Skill;