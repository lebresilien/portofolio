import React from "react";
import { useTranslation } from "react-i18next";

const Footer = () => {

    const { t } = useTranslation('common');

    return (
        <div className="mt-12 lg:mt-18 sm:pb-10 sm:py-12 py-6">
            <div className="max-w-7xl px-4 mx-auto text-gray-800 dark:text-white">
                <div className="pb-8 mb-2 border-t-2 border-gray-300 dark:border-white-300"></div>
                <div className="flex flex-col justify-between lg:flex-row items-center">
                    <p>
                      &#169; {t('portofolio.copyright')}
                    </p>

                    <div className="flex flex-wrap pt-2 sm:space-x-4 space-x-2 font-medium lg:pt-0">
                        <a
                            href="https://linkedin.com/in/petito-tapondjou-3803b915b"
                            className={"transition-colors hover:text-[#2F1C6A]"}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Linkedin
                        </a>
                        <a
                            href="https://github.com/lebresilien"
                            className={"transition-colors hover:text-[#2F1C6A]"}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Gitlab
                        </a>
                        <a
                            href="https://twitter.com/lebresilien237"
                            className={"transition-colors hover:text-[#2F1C6A]"}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Twitter
                        </a>
                       
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;