import React from "react";
import { useTranslation } from "react-i18next";

const Project = () => {

    const { t } = useTranslation("common");

    const projects = [

        {
            title: "Portofolio",
            description: t('portofolio.projects.desc_1'),
            link: "https://gitlab.com/lebresilien/portofolio",
            techs: ["next js", "tailwindcss"]
        },
        {
            title: "Faseya",
            description: t('portofolio.projects.desc_6'),
            link: "http://faseya.com",
            techs: ["node js", "react js", "mdbootstrap"]
        },
        {
            title: "Benss",
            description: t('portofolio.projects.desc_4'),
            link: "https://gitlab.com/lebresilien/benss-mobile",
            techs: ["laravel", "react native"]
        },
        {
            title: "Afrosbe",
            description: t('portofolio.projects.desc_2'),
            link: "http://afrosbe.com",
            techs: ["php", "jquery", "bootstrap", "css"]
        },
        {
            title: "Scolarships",
            description: t('portofolio.projects.desc_3'),
            link: "https://gitlab.com/lebresilien/school",
            techs: ["node js", "react native", "bootstrap"]
        },
        {
            title: "Grow-up",
            description: t('portofolio.projects.desc_5'),
            link: "https://gitlab.com/lebresilien/grow-up",
            techs: ["laravel", "react js", "bootstrap"]
        },
    ];

    const ProjectCard = ({title, description, link, techs}) => (
        <div className="flex flex-col my-5 mr-3 md:w-96 items-center rounded border-solid border-2 text-[#2F1C6A] dark:text-white">

            <div className="flex flex-row justify-center my-3">
                <h3 className="text-3xl dark:text-white md:text-4xl">
                    { title }
                </h3>
            </div>

            <div className="flex flex-row my-3 justify-center">
                <p className="text-2xl pl-4 pr-1">
                    { description }
                </p>
            </div>

            <div className="flex flex-row my-2 justify-center">
                <h6 className="text-2xl dark:text-white md:text-2xl text-xl">
                    {t('portofolio.projects.text_tech')}
                </h6>
            </div>

            <div className="flex flex-row justify-center my-2 pl-4 pr-1">
                
                {techs.map((item, index) => (
                    <span className="text-lg mx-1" key={index}>{item}</span>
                ))}

            </div>

            <div className="flex flex-row justify-center my-2">
                <button className="rounded-full bg-[#2F1C6A] text-white p-2 hover:font-bold">
                    <a href={link} target="_blank" rel="noreferrer">
                        {t('portofolio.projects.text_code')}
                    </a>
                </button>
            </div>
        </div>
    )

    return (
        <div className="container px-4 mx-auto">
            <div className="lg:space-x-5 item-center lg:-mx-4 flex flex-col">

                <div className="lg:px-4 lg:mt-12">
                    <h1 className="text-2xl font-bold text-[#2F1C6A] lg:text-5xl dark:text-white">
                        {t('portofolio.projects.title')}
                    </h1>
                </div>

                <div className="flex md:flex-row md:flex-wrap flex-col my-5 justify-between">
                   
                   {
                      projects.map((project, index) => (
                        <ProjectCard 
                           title={project.title}
                           description={project.description}
                           link={project.link}
                           techs={project.techs}
                           key={index}
                        />
                      )) 
                   }
                   
                </div>

            </div>
        </div>
    )
}

export default Project;