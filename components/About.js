import React from "react"
import Image from "next/image"
import profile from "../public/img/profile.jpeg";
import { useTranslation } from 'react-i18next';

const About = () => {

    const { t } = useTranslation('common');

    return (
        <div className="container px-4 mx-auto text-[#2F1C6A]">
            <div className="lg:space-x-5 lg:flex lg:flex-row item-center lg:-mx-4 flex flex-col-reverse text-center lg:text-left">
                <div className="lg:px-4 lg:mt-12 ">
                    <h1 className="text-2xl font-bold lg:text-5xl dark:text-white">
                     {t('portofolio.about.greeting')}
                    </h1>
                    <div className="mt-6 dark:text-white md:text-2xl text-xl">
                        <p className="mb-4">
                        {t('portofolio.about.bio')}
                        </p>
                    </div>
                </div>
                <div className="flex-shrink-0 lg:mt-12 lg:px-4 mb-10">
                    <Image
                        src={profile}
                        alt="Profile"
                        priority={true}
                        className="rounded-full"
                        width={250}
                        height={250}
                        placeholder="blur"
                    />
                </div>
            </div>
        </div>
    )
}

export default About;
