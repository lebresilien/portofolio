import { DiAndroid, DiTerminal, DiApple } from "react-icons/di";
import { SiPhp, SiMysql, SiGit, SiTrello } from "react-icons/si";


const data = {
  
    skills: [
        {
            slug: "android",
            Component: DiAndroid,
            title: "Android",
            Description: () => <>I work on ROMs, Android Apps using React Native.</>,
          },
          {
            slug: "apple",
            Component: DiApple,
            title: "Apple",
            Description: () => <>I work on ROMs, Apple Apps using React Native.. </>,
          },
          {
            slug: "terminal",
            Component: DiTerminal,
            title: "Web developpment",
            Description: () => <>I have written many web apps for  various purposes with Frameworks like Laravel, React js and Node Js.</>,
          },
          {
            slug: "Mysql",
            Component: SiMysql,
            title: "Mysql",
            Description: () => <>I have used Mysql to perform the apps data.</>,
          }, 
          {
            slug: "trello",
            Component: SiTrello,
            title: "Trello",
            Description: () => <>I have used Trello for planified, following & managing the projets.</>,
          },
          {
            slug: "git",
            Component: SiGit,
            title: "Git",
            Description: () => <>Git is a tool that I use every day. I use Gitlab for pushing my code.</>,
          },
    ]
}

export default data;